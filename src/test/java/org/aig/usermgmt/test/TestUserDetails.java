package org.aig.usermgmt.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.aig.usermgmt.dao.UserDao;
import org.aig.usermgmt.daoimpl.UserDaoImpl;
import org.aig.usermgmt.model.User;
import org.aig.usermgmt.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author pkumar1
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/applicationContext.xml")
public class TestUserDetails {
	UserDao userDao = new UserDaoImpl();
	@Autowired
	UserRepository userRepository;

	@Test
	public void checkGetUser() {
		User user = userRepository.findOne(1);
		assertNotNull(user);
	}

	@Test
	public void checkAddUser() {

		User user = new User("pradeepaero07@gmail.com", "Pradeep Kumar K", 9902236072L, "Male");
		userRepository.save(user);
		User user1 = userRepository.findOne(user.getUserId());
		assertNotNull(user1);
	}

	@Test
	public void checkGetAllUser() {
		List<User> userList = new ArrayList<>();
		userList = userRepository.findAll();
		assertNotNull(userList);
	}

//	@Test
//	public void checkdeleteUser() {
//
//		userDao.deleteUser(2);
//		User user1 = userRepository.findOne(2);
//		assertNull(user1);
//
//	}

	@Test
	public void checkUpdateUser() {
		User user = new User("pradeepaero07@gmail.com", "Pradeep Kumar K", 9902236072L, "Male");
		userDao.updateUser(5, user);
		User user1 = userRepository.findOne(5);
		boolean value = findEquality(user, user1);
		assertFalse(value);
	}

	public boolean findEquality(User user, User user1) {

		if (user.getEmailId().equals(user1.getEmailId()) && user.getGender().equals(user1.getGender())
				&& user.getName().equals(user1.getName()) && user.getPhoneNo() == user1.getPhoneNo()
				&& user.getUserId() == user1.getUserId()) {
			return true;
		}

		else {
			return false;
		}
	}

}
