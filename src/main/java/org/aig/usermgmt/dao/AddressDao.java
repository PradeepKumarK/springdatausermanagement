package org.aig.usermgmt.dao;

import java.util.List;

import org.aig.usermgmt.model.Address;

/**
 * @author pkumar1
 *
 */
public interface AddressDao {

	List<Address> getAllAddresses(int userId);

	Address getAddress(int addressId, int addressId2);

	Address addAddress(int userId, Address address);

	Address updateAddress(int userId, Address address, int addressId);

	void deleteAddress(int addressId, int addressId2);

}
