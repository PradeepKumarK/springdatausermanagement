package org.aig.usermgmt.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.aig.usermgmt.model.Address;
import org.aig.usermgmt.services.AddressService;
import org.aig.usermgmt.servicesimpl.AddressServiceImpl;

/**
 * @author pkumar1
 *
 */
public class AddressResource {

	AddressService addressService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Address> getAllUsers(@PathParam("userId") int userId) {
		addressService = new AddressServiceImpl();
		return addressService.getAllAddresses(userId);
	}

	@GET
	@Path("/{addressId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Address getAddress(@PathParam("userId") int userId, @PathParam("addressId") int addressId) {
		addressService = new AddressServiceImpl();
		return addressService.getAddress(userId, addressId);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Address addAddress(@PathParam("userId") int userId, Address address) {
		addressService = new AddressServiceImpl();
		return addressService.addAddress(userId, address);

	}

	@PUT
	@Path("/{addressId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Address updateAddress(@PathParam("userId") int userId, @PathParam("addressId") int addressId,
			Address address) {
		addressService = new AddressServiceImpl();
		return addressService.updateAddress(userId, address, addressId);
	}

	@DELETE
	@Path("/{addressId}")
	@Produces(value={MediaType.TEXT_PLAIN,MediaType.APPLICATION_JSON})
	public String deleteAddress(@PathParam("userId") int userId, @PathParam("addressId") int addressId) {

		addressService = new AddressServiceImpl();
		addressService.deleteAddress(userId, addressId);
		return "the address with id : " + addressId + " for the user :" + userId + "is deleted";

	}

}
