package org.aig.usermgmt.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.aig.usermgmt.model.User;
import org.aig.usermgmt.services.UserService;
import org.aig.usermgmt.servicesimpl.UserServiceImpl;

/**
 * @author pkumar1
 *
 */
@Path("/userdetails")
public class UserResource {

	UserService userService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getAllUsers() {
		userService = new UserServiceImpl();
		return userService.getAllUsers();
	}

	@GET
	@Path("/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("userId") int id) {
		userService = new UserServiceImpl();
		return userService.getUser(id);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User addUser(User user) {
		// System.out.println(user.getName());
		userService = new UserServiceImpl();
		return userService.addUser(user);
		// return user;
	}

	@PUT
	@Path("/{userId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User updateUser(@PathParam("userId") int id, User user) {
		// System.out.println(user.getName());
		// return user;
		userService = new UserServiceImpl();
		return userService.updateUser(id, user);
	}

	@DELETE
	@Path("/{userId}")
	@Produces(value={MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String deleteUser(@PathParam("userId") int id) {
		userService = new UserServiceImpl();
		userService.deleteUser(id);
		return "The User with the id: " + id + " is deleted";
	}

	@Path("/{userId}/addresses")
	public AddressResource getAddressResource()
	{
		return new AddressResource();
	}
	
	
}