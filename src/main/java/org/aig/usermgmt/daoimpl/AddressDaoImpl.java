package org.aig.usermgmt.daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.aig.usermgmt.dao.AddressDao;
import org.aig.usermgmt.exception.DataNotFoundException;
import org.aig.usermgmt.model.Address;
import org.aig.usermgmt.model.ErrorMessage;
import org.aig.usermgmt.repositories.AddressRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author pkumar1
 *
 */

public class AddressDaoImpl implements AddressDao {
	List<Address> addressList;
	static Logger log = Logger.getLogger(AddressDaoImpl.class);
	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("META-INF/applicationContext.xml");

	@Autowired
	AddressRepository addressRepository = applicationContext.getBean(AddressRepository.class);;

	/**
	 * @param userId
	 *            An Id to get the addresses of a particular user
	 * @return addressList List of addresses of a particular user
	 */

	@Override
	public List<Address> getAllAddresses(int userId) {

		List<Address> addressList = addressRepository.findByUserId(userId);

		if (addressList.size() == 0) {
			throw new DataNotFoundException("No Content", null);
		}

		return addressList;

	}

	/**
	 * @param userId
	 *            An Id to get the addresses of a particular user
	 * @param addressId
	 *            An Id to get the particular address of a user
	 * @return address Address of a user
	 */

	@Override
	public Address getAddress(int userId, int addressId) {
		addressList = new ArrayList<>();
		Response response = getResponse();

		addressList = addressRepository.findByUserId(userId);
		if (addressList.size() == 0) {
			throw new WebApplicationException(response);
		}

		for (Address address : addressList) {
			if (address.getId() == addressId) {
				return address;
			}
		}

		return null;
	}

	/**
	 * @param userId
	 *            An Id to get the addresses of a particular user
	 * @param address
	 *            An address which is going to be added to a user
	 * @return address Address of a user
	 */

	@Override
	public Address addAddress(int userId, Address address) {
		address.setUserId(userId);
		addressRepository.save(address);
		return address;

	}

	/**
	 * @param userId
	 *            An Id to get the addresses of a particular user
	 * @param address
	 *            An address which is going to be added to a user
	 * @param addressId
	 *            An Id to get the particular address of a user
	 * @return address Address of a user
	 */

	@Override
	public Address updateAddress(int userId, Address address, int addressId) {
		Response response = getResponse();
		addressList = new ArrayList<>();
		addressList = addressRepository.findByUserId(userId);
		if (addressList.size() == 0) {
			throw new WebApplicationException(response);
		} else {
			for (Address address2 : addressList) {
				if (address2.getId() == addressId) {
					Address address1 = addressList.get(0);
					address1.setCity(address.getCity());
					address1.setPinCode(address.getPinCode());
					address1.setState(address.getState());
					address1.setStreet(address.getStreet());
					address1.setUserId(address.getUserId());
					addressRepository.save(address2);
					return address2;
				}
			}
		}
		return null;
	}

	/**
	 * @param userId
	 *            An Id to get the addresses of a particular user
	 * @param addressId
	 *            An Id to delete the particular address of a user
	 */

	@Override
	public void deleteAddress(int userId, int addressId) {
		Response response = getResponse();
		addressList = addressRepository.findByUserId(userId);
		if (addressList.size() == 0) {
			throw new WebApplicationException(response);
		} else {

			for (Address address2 : addressList) {
				if (address2.getId() == addressId) {
					addressRepository.delete(address2);
					break;
				}

			}
		}
	}


	/**
	 * This method is used to get the response along with the "Not Found" error
	 * message
	 */
	public Response getResponse() {
		ErrorMessage errorMessage = new ErrorMessage("Not Found", 500, "http://www.google.co.in");
		Response response = Response.status(Status.NOT_FOUND).entity(errorMessage).build();
		return response;
	}

}
