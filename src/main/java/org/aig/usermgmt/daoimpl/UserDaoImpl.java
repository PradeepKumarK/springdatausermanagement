package org.aig.usermgmt.daoimpl;

import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.aig.usermgmt.dao.UserDao;
import org.aig.usermgmt.exception.DataNotFoundException;
import org.aig.usermgmt.model.ErrorMessage;
import org.aig.usermgmt.model.User;
import org.aig.usermgmt.repositories.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author pkumar1
 *
 */

public class UserDaoImpl implements UserDao {

	static Logger log=Logger.getLogger(UserDaoImpl.class);
	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("META-INF/applicationContext.xml");

	@Autowired
	UserRepository userRepository = applicationContext.getBean(UserRepository.class);

	Response response;

	/**
	 * @return userList1 The list of the users
	 *
	 */
	@Override
	public List<User> getAllUsers() {
		response = getErrorMessageNoContent();

		List<User> userList1 = userRepository.findAll();
		log.info("the size of the userlist is "+userList1.size());
		if (userList1.size() == 0) {
			throw new DataNotFoundException(response);
		}

		return userList1;
	}

	/**
	 * @Param user The details of a particular user
	 * @return user The details of a particular user
	 *
	 */
	@Override
	public User addUser(User user) {
		userRepository.save(user);
		return user;
	}

	/**
	 * @param id The id of a user
	 * @param user The details of a particular user to update
	 * @return user The details of a updated user
	 */
	@Override
	public User updateUser(int id, User user) {
		Response response = getErrorMessgeNotFound();
		User user1 =userRepository.findOne(id);
		if (user1 == null) {
			throw new WebApplicationException(response);
		} else {
			user1.setEmailId(user.getEmailId());
			user1.setGender(user.getGender());
			user1.setPhoneNo(user.getPhoneNo());
			user1.setName(user.getName());
			userRepository.save(user1);
		}
		return user1;
	}

	/**
	 * @param id The id of a user to delete the user
	 */
	@Override
	public void deleteUser(int id) {
		Response response = getErrorMessgeNotFound();
		User user1 =userRepository.findOne(id);
		if (user1 == null) {
			throw new WebApplicationException(response);
		} else {
			userRepository.delete(user1);
		}
	}

	/**
	 * @param id The id of a user to get the user
	 * @return
	 */
	@Override
	public User getUser(int id) {
		Response response = getErrorMessgeNotFound();
		User user = userRepository.findOne(id);

		if (user == null) {
			throw new WebApplicationException(response);
		}
		return user;
	}


	/**
	 * @return Response A response along with the error message "No Content"
	 */
	public Response getErrorMessageNoContent() {
		ErrorMessage errorMessage = new ErrorMessage("No Content", 201, "http://www.google.co.in");
		Response response = Response.status(Status.NO_CONTENT).entity(errorMessage).build();
		return response;
	}

	/**
	 * @return Response A response along with the error message "Not Found"
	 */
	public Response getErrorMessgeNotFound() {
		ErrorMessage errorMessage = new ErrorMessage("Not Found", 500, "http://www.google.co.in");
		Response response = Response.status(Status.NOT_FOUND).entity(errorMessage).build();
		return response;
	}


}
