package org.aig.usermgmt.servicesimpl;

import java.util.List;

import org.aig.usermgmt.dao.AddressDao;
import org.aig.usermgmt.daoimpl.AddressDaoImpl;
import org.aig.usermgmt.model.Address;
import org.aig.usermgmt.services.AddressService;

/**
 * @author pkumar1
 *
 */
public class AddressServiceImpl implements AddressService {

	@Override
	public List<Address> getAllAddresses(int userId) {
		AddressDao addressDaoObj = new AddressDaoImpl();
		return addressDaoObj.getAllAddresses(userId);
	}

	@Override
	public Address getAddress(int userId,int addressId) {
		AddressDao addressDaoObj = new AddressDaoImpl();
		return addressDaoObj.getAddress(userId,addressId);
	}

	@Override
	public Address addAddress(int userId,Address address) {
		AddressDao addressDaoObj = new AddressDaoImpl();
		return addressDaoObj.addAddress(userId,address);
	}

	@Override
	public Address updateAddress(int userId, Address address,int addressId) {
		AddressDao addressDaoObj = new AddressDaoImpl();
		return addressDaoObj.updateAddress(userId,address,addressId);
	}

	@Override
	public void deleteAddress(int userId,int addressId) {
		AddressDao addressDaoObj = new AddressDaoImpl();
		addressDaoObj.deleteAddress(userId,addressId);		
	}

}
