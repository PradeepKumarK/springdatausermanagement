package org.aig.usermgmt.servicesimpl;

import java.util.List;

import org.aig.usermgmt.dao.UserDao;
import org.aig.usermgmt.daoimpl.UserDaoImpl;
import org.aig.usermgmt.model.User;
import org.aig.usermgmt.services.UserService;

/**
 * @author pkumar1
 *
 */
public class UserServiceImpl implements UserService{

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		UserDao userDao=new UserDaoImpl();
		return userDao.getAllUsers();
	}

	@Override
	public User addUser(User user) {
		UserDao userDao=new UserDaoImpl();
		return userDao.addUser(user);
	}

	@Override
	public User updateUser(int id, User user) {
		// TODO Auto-generated method stub
		UserDao userDao=new UserDaoImpl();
		return userDao.updateUser(id,user);
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		UserDao userDao=new UserDaoImpl();
		userDao.deleteUser(id);
	}

	@Override
	public User getUser(int id) {
		UserDao userDao=new UserDaoImpl();
		return userDao.getUser(id);
	}


}
