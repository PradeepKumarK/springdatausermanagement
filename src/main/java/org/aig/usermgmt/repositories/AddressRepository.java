package org.aig.usermgmt.repositories;

import java.util.List;

import org.aig.usermgmt.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author pkumar1
 *
 */
public interface AddressRepository extends JpaRepository<Address, Integer> {

	/**
	 * @param userId An id used to get the addresses of a particular user
	 * @return List of addresses
	 */
	@Query("SELECT a FROM Address a WHERE a.userId=:userId")
	    public List<Address> findByUserId(@Param("userId") int userId);
}

