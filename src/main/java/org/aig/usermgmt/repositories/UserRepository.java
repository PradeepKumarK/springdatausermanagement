package org.aig.usermgmt.repositories;

import org.aig.usermgmt.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

}
