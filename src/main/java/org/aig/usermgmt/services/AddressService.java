package org.aig.usermgmt.services;

import java.util.List;

import org.aig.usermgmt.model.Address;
import org.aig.usermgmt.model.User;

/**
 * @author pkumar1
 *
 */
public interface AddressService {

	List<Address> getAllAddresses(int userId);

	Address getAddress(int addressId, int addressId2);

	Address addAddress(int userId, Address address);

	Address updateAddress(int userId, Address address, int addressId);

	void deleteAddress(int addressId, int addressId2);

}
