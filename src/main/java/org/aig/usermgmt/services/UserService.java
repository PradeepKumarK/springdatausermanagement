package org.aig.usermgmt.services;

import java.util.List;

import org.aig.usermgmt.model.User;

/**
 * @author pkumar1
 *
 */
public interface UserService {

	List<User> getAllUsers();

	User addUser(User user);

	User updateUser(int id, User user);

	void deleteUser(int id);

	User getUser(int id);

}
